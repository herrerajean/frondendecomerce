import { Directive ,HostListener,EventEmitter,Input,Output} from '@angular/core';

@Directive({
  selector: '[appNgDropFiles]'
})
export class NgDropFilesDirective {

  @Input() archivos:File[]=[];

  @Output() mouseSobre:EventEmitter<boolean> = new EventEmitter();

  @Output() sizefile:EventEmitter<number> = new EventEmitter();

  constructor() { }

  @HostListener('dragover',['$event'])
  public elementoSobreImagen(event : any){
    this.mouseSobre.emit(true);
    this._prevenirDetener( event );
  }

  @HostListener('dragleave',['$event'])
  public elementoFueraImagen(event : any){
    this.mouseSobre.emit(false)
  }

  @HostListener('drop', ['$event'])
  public onDrop( event: any ) {

    const transferencia = this._getTransferencia( event );

    if ( !transferencia ) {
      return;
    }

    console.log(transferencia.files+" ESTO ES LA TRANSFERENCIA")

    this._extraerArchivos( transferencia.files );
    this._prevenirDetener( event );
    this.mouseSobre.emit( false );
    
    console.log(this.archivos.length)
    this.sizefile.emit(this.archivos.length)
  }

  private _getTransferencia( event: any ) {
    return event.dataTransfer ? event.dataTransfer : event.originalEvent.dataTransfer;
  }

  private _extraerArchivos( archivosLista: FileList ) {

    console.log(archivosLista)
    for ( let propiedad in Object.getOwnPropertyNames( archivosLista )  ) {
      
      const archivoTemporal = archivosLista[propiedad];
      
      if ( this._archivoPuedeSerCargado( archivoTemporal ) ) {

        const nuevoArchivo = archivoTemporal ;
        this.archivos.push( nuevoArchivo );

      }
    }
    console.log(this.archivos)
  }


  // Validaciones
  private _archivoPuedeSerCargado( archivo: File ): boolean {

    if ( !this._archivoYaFueDroppeado( archivo.name ) && this._esImagen( archivo.type ) ){
      return true;
    }else {
      return false;
    }

  }


  private _prevenirDetener( event ) {
    event.preventDefault();
    event.stopPropagation();
  }

  private _archivoYaFueDroppeado( nombreArchivo: string ): boolean {

    for ( const archivo of this.archivos ) {

      if ( archivo.name === nombreArchivo  ) {
        console.log('El archivo ' + nombreArchivo + ' ya esta agregado');
        return true;
      }

    }

    return false;
  }

  private _esImagen( tipoArchivo: string ): boolean {
    return ( tipoArchivo === '' || tipoArchivo === undefined ) ? false : tipoArchivo.startsWith('image');
  }


}