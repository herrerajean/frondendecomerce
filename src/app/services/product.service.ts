import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Producto} from '../model/producto.model'

//Firebase
import {AngularFirestore} from '@angular/fire/firestore'
import * as firebase from 'firebase'

@Injectable()
export class ProductoService {
    
    urlImagenProducto:string="";

    constructor(private httpClient:HttpClient,private db:AngularFirestore){
        console.log("INICIANDO SERVICE")
    }
    
    private producto:Producto []=[];

    url=`http://localhost:8080/`;



    getProducto(){
        return this.httpClient.get<Producto[]>(this.url+`product/listproducts`);
    }

    getProductsByName(query:string){
        return this.httpClient.get<Producto[]>(this.url+`product/search?q=${query}`);
    }


    getProductByName(name:string){
        return this.httpClient.get<Producto>(this.url+`product/nameproduct?name=${name}`);
    }

    
    getProductByCategory(categoria:string){
        return this.httpClient.get<Producto[]>(this.url+`product/category?id=${categoria}`);
    }

    saveProduct(producto:Producto){
        return this.httpClient.post<Producto>(this.url+`product/save`,producto);
    }

    deleteProduct(id:number){
        return this.httpClient.delete<Producto>(this.url+`product/delete?id=${id}`)
    }
    
    private Carpeta_Imagenes='img'

    cargarImagenes(imagenes:File[],p:Producto){
        console.log("ENTRO A IMAGENES")
        const storageRef = firebase.storage().ref();
        for(const item of imagenes){
            
            const uploadTask: firebase.storage.UploadTask = 
                            storageRef.child(`${this.Carpeta_Imagenes}/${item.name}`)
                                                .put(item);
            console.log(item);
            uploadTask.on( firebase.storage.TaskEvent.STATE_CHANGED,
                        ( snapshot: firebase.storage.UploadTaskSnapshot ) => console.log(snapshot),
                        (error)=>console.error("Error al subir"+error),
                        ()=>{
                            console.log("ENTRO AL VACIO")
                            uploadTask.snapshot.ref.getDownloadURL().then( data =>{
                                let url = data;
                                let nombre = item.name;
                                this.guardarImagenes({
                                    nombre:nombre,url:url
                                },p)
                                
                            })
                        })
        }
    }
    
    private guardarImagenes(imagen:{nombre:string,url:string},p:Producto){
        this.db.collection(`/${this.Carpeta_Imagenes}`)
                    .add(imagen);
        p.url=imagen.url;
        this.saveProduct(p).subscribe()
    }
    
}