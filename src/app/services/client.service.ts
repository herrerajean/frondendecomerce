import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Cliente } from '../model/cliente.model';

@Injectable()
export class ClienteService {
    
    url=`http://localhost:8080/client/`
    
    constructor(private httpCliente:HttpClient){
    
    }

    getClientes(){
        return this.httpCliente.get<Cliente[]>(this.url+`listclientes`);
    }

}