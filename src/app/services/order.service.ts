import { Injectable, EventEmitter } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {PedidoProducto} from '../model/pedido_producto.model'
import {map} from 'rxjs/operators'

@Injectable()
export class  PedidoService{
    
    constructor(private httpClient: HttpClient){ 

    }
    
    url= `http://localhost:8080/order/`

    savePedido(pedido:PedidoProducto){
        return this.httpClient.post<PedidoProducto>(this.url+`save`,pedido);
    }

    getPedido(){
        return this.httpClient.get<PedidoProducto[]>(this.url+`listorders`);
    }

    deletePedido(id:number){
        return this.httpClient.delete<PedidoProducto>(this.url+`delete?id=${id}`)
    }

    getCantidad(clienteId:number){
        return this.httpClient.get<number>(this.url+`cantidad?id=${clienteId}`);
    }

    carrito = new EventEmitter<number>();

    loading = new EventEmitter<boolean>();

    

}