import { Distrito } from './distrito.model'

export class Cliente{
    idCliente:number
    nombreCliente:string
    apellidoCliente:string
    sexo:string
    direccion:string
    celular:string
    email:string
    documentoId:string
    fechaRegistro:string
    estadoCivil:string
    distrito:Distrito

    constructor(){
        this.idCliente=1;
        this.nombreCliente="";
        this.apellidoCliente="";
        this.sexo="";
        this.direccion="";
        this.celular="";
        this.email="";
        this.documentoId="";
        this.fechaRegistro="";
        this.estadoCivil="";
        this.distrito= new Distrito();
    }
}