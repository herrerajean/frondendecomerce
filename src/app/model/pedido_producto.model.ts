import { Producto } from "./producto.model"
import { Trabajador } from './trabajador.model'
import { Pedido } from './pedido.model'

export class PedidoProducto{
    idPedidoProducto:number
    cantidad:number
    descripcion:string
    precioUnitario:number
    precioTotal:number
    pedido:Pedido
    producto:Producto

    constructor(){
        this.idPedidoProducto=0
        this.cantidad=0
        this.descripcion=""
        this.precioUnitario=0
        this.precioTotal=0
        this.pedido= new Pedido()
        this.producto= new Producto()
    }
}