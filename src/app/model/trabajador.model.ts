import { Cargo } from "./cargo.model"
import { Distrito } from './distrito.model'

export class Trabajador{
    idTrabajador:number
    nombreTrabajador:string
    apellidoTrabajador:string
    sexo:string
    area:string
    turno:string
    salario:number
    fechaInicio:string
    fechaSese:string
    direccion:string
    celular:string
    email:string
    documentoId:string
    estadoVendedor:string
    estadoCivil:string
    distrito:Distrito
    cargo:Cargo

    constructor(){
        this.idTrabajador=0
        this.nombreTrabajador=""
        this.apellidoTrabajador=""
        this.sexo=""
        this.area=""
        this.turno=""
        this.salario=0
        this.fechaInicio=""
        this.fechaSese=""
        this.direccion=""
        this.celular=""
        this.email=""
        this.documentoId=""
        this.estadoVendedor=""
        this.estadoCivil=""
        this.distrito= new Distrito()
        this.cargo=new Cargo()
    }
}