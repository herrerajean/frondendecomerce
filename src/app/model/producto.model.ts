import { Categoria } from './categoria.model';

export class Producto{

    id_producto:number;
    nombre_producto:string;
    cantidad:number;
    precio:number;
    estado:string;
    url:string;
    fecha_registro:string;
    categoria:Categoria;

    constructor(){
        this.id_producto=0;
        this.nombre_producto="";
        this.cantidad=0;
        this.estado="";
        this.fecha_registro="";
        this.url=";"
        this.categoria=new Categoria();
    }
    
}