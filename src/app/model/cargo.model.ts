export class Cargo{
    idCargo:number
    nombreCargo:string
    fechaCreacion:string
    estadoCargo:string

    constructor(){
        this.idCargo=0
        this.nombreCargo=""
        this.fechaCreacion=""
        this.estadoCargo=""
    }
}