import { Cliente } from './cliente.model'

export class Pedido{
    idPedido:number
    fechaPedido:string
    estado:string
    cliente:Cliente

    constructor(){
        this.idPedido=0;
        this.fechaPedido="";
        this.estado="";
        this.cliente= new Cliente();
    }
}