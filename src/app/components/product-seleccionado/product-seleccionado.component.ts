import { Component, EventEmitter, OnInit } from "@angular/core";
import { Producto } from "src/app/model/producto.model";
import { ProductoService } from "src/app/services/product.service";
import { ActivatedRoute } from "@angular/router";
import { PedidoService } from "src/app/services/order.service";
import { PedidoProducto } from "src/app/model/pedido_producto.model";

@Component({
  selector: "app-product-seleccionado",
  templateUrl: "./product-seleccionado.component.html",
  styleUrls: ["./product-seleccionado.component.css"]
})
export class ProductSeleccionadoComponent implements OnInit {
  increment: number = 1;

  loading: boolean = false;

  producto: Producto = new Producto();
  pedidoModel: PedidoProducto = new PedidoProducto();
  pedidos: PedidoProducto[] = [];
  descripcion;
  cantidad;
  contador: number = 0;
  totalCantidad;

  constructor(
    private httpClient: ProductoService,
    private activatedRoute: ActivatedRoute,
    private pedidoService: PedidoService
  ) {
    this.pedidoService.getPedido().subscribe(data => {
      this.pedidos = data;
    });
    console.log(this.contador);
  }

  ngOnInit() {
    console.log(this.pedidos);
    this.activatedRoute.params.subscribe(param => {
      this.httpClient.getProductByName(param["name"]).subscribe(data => {
        this.producto = data;
      });
    });
  }


  agregarCarrito() {
    console.log(this.contador);
    this.loading = true;
    this.pedidoService.loading.emit(this.loading);

    if (
      (this.getPedidos().length > 0 && this.contador == 2) ||
      (this.getPedidos().length > 0 && this.contador == 0)
    ) {
      console.log("ENTRO");
      let i = 0;
      let cantidad = 0;
      for (let p of this.pedidos) {
        i++;
        console.log(
          "ESTO ES CANTIDADES: " +
            cantidad +
            " ESTO ES P.CANT: " +
            p.cantidad +
            "SUMA: ",
          cantidad + p.cantidad
        );
        if (
          p.producto.id_producto == this.producto.id_producto &&
          p.pedido.cliente.idCliente == 1
        ) {
          this.save(p, p.idPedidoProducto, p.pedido.idPedido);
          break;
        }
        if (i == this.getPedidos().length) {
          this.save(this.pedidoModel, 0);
        }
      }
    }
    setTimeout(() => {
      this.loading = false;
      if (this.getPedidos().length == 0 && this.contador == 0) {
        this.contador = 2;
        this.save(this.pedidoModel, 0);
      }
      this.pedidoService.loading.emit(this.loading);
    }, 2000);
    
  }


  save(p: PedidoProducto, id?: number, id_pedido?: number) {
    p.idPedidoProducto = id;
    p.producto = this.producto;
    p.pedido.fechaPedido = "2019-01-21";
    p.pedido.estado = "Activo";
    console.log(
      p.cantidad + "CANTIDADDD ",
      p.cantidad + this.increment + " CANTIDAD increment"
    );
    p.cantidad = p.cantidad + this.increment;
    p.precioUnitario = this.producto.precio;
    p.precioTotal = this.producto.precio * p.cantidad;
    p.pedido.idPedido = id_pedido;
    this.pedidoService.savePedido(p).subscribe(data => console.log(data));
    this.pedidoService.carrito.emit(this.cantPedidos(p.cantidad,id));
    console.log(this.pedidos)
  }


  cantPedidos(cant:number,id:number){
    this.cantidad=0;
    console.log(this.pedidos)
    if(this.getPedidos().length>0){
      for(let p of this.getPedidos()){
        this.cantidad+=p.cantidad;
      }
      if(id==0){
        return this.cantidad+cant;
      }else{
        return this.cantidad;
      }
    }else {
      return cant;
    }
  }


  incrementMethod(operacion: string) {
    if (operacion == "+") {
      this.increment = this.increment + 1;
    } else {
      if (this.increment > 1) {
        this.increment = this.increment - 1;
      }
    }
  }

  getPedidos() {
    this.pedidoService.getPedido().subscribe(data => {
      this.pedidos = data;
    });
    return this.pedidos;
  }
}
