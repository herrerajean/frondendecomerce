import { Component, OnInit } from '@angular/core';
import { PedidoService } from 'src/app/services/order.service';
import { PedidoProducto } from 'src/app/model/pedido_producto.model';

@Component({
  selector: 'app-carrito-compras',
  templateUrl: './carrito-compras.component.html',
  styleUrls: ['./carrito-compras.component.css']
})
export class CarritoComprasComponent {

  pedidos:PedidoProducto[]=[];
  cantidadPedido:number=0;
  constructor(private pedidoService: PedidoService) { 
    this.pedidoService.getPedido().subscribe( data => {
      this.pedidos= data;
      console.log(this.pedidos.length+"CONSTRUCTOR CARRITO")
    });
    this.pedidoService.getCantidad(1).subscribe(data => this.cantidadPedido=data);
  }
  
  deletePedido(id:number,index?:number){
    console.log(this.cantidadPedido)
    this.pedidoService.deletePedido(id).subscribe()
    this.cantidadPedido-=this.pedidos[index].cantidad
    this.pedidoService.carrito.emit(this.cantidadPedido)
    this.pedidos.splice(index,1)
  }

}
