import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { ProductoService } from 'src/app/services/product.service';
import { Producto } from 'src/app/model/producto.model';
import {  Router } from '@angular/router';
import { PedidoService } from 'src/app/services/order.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  consultas:string;

  productos: any= {};

  product:Producto;

  numberCart:number;

  loading:boolean=false;

  constructor(private pedidoService:PedidoService, private httpClient? : ProductoService,private route? : Router) { 
    
    this.pedidoService.carrito.subscribe(number => this.numberCart=number);
    this.pedidoService.loading.subscribe(result => this.loading=result);

    this.product= new Producto;
  }
    
  

   buscar(query:string){
    this.httpClient.getProductsByName(query).subscribe((data:any) =>{
      this.productos=data;
    });
    
   }
    
   guardar(){
     this.httpClient.saveProduct(this.product).subscribe(
       data =>{
          alert("se agrego producto exitosamente");
          this.route.navigate(['/category','audios']);
       }
     )
   }

}
