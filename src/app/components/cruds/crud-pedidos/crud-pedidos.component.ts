import { Component, OnInit } from '@angular/core';
import { PedidoService } from 'src/app/services/order.service';
import { PedidoProducto } from 'src/app/model/pedido_producto.model';
import { Cliente } from 'src/app/model/cliente.model';
import { ClienteService } from 'src/app/services/client.service';
import { ProductoService } from 'src/app/services/product.service';
import { Producto } from 'src/app/model/producto.model';

@Component({
  selector: 'app-crud-pedidos',
  templateUrl: './crud-pedidos.component.html',
  styleUrls: ['./crud-pedidos.component.css']
})
export class CrudPedidosComponent {

  pedidos:PedidoProducto[]=[];

  pedidoModel:PedidoProducto;

  clientes:Cliente[]=[];

  productos:Producto[]=[];

  constructor(private pedidoService:PedidoService,private productService:ProductoService,private clienteService:ClienteService) { 
    this.pedidoService.getPedido().subscribe(data => {
      this.pedidos=data;
    });
    this.productService.getProducto().subscribe(data =>{
      this.productos = data;
    });
    this.clienteService.getClientes().subscribe(data => {
      this.clientes = data;
    });
    this.pedidoModel= new PedidoProducto();
  }

  acciones(accion:string,p:PedidoProducto,comodin?:number){
    if(accion == 'editar'){
      p.precioTotal=comodin;
      console.log(p.precioTotal);
      this.pedidoService.savePedido(p).subscribe();
    }
    if(accion == 'eliminar'){
      this.pedidoService.deletePedido(p.idPedidoProducto).subscribe(()=> this.pedidos.splice(comodin,1))
    }
  }

}
