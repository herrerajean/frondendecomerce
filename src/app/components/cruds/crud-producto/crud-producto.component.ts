import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/model/producto.model';
import { Router } from '@angular/router';
import { ProductoService } from 'src/app/services/product.service';
import { Categoria } from 'src/app/model/categoria.model';

@Component({
  selector: 'app-crud-producto',
  templateUrl: './crud-producto.component.html',
  styleUrls: ['./crud-producto.component.css']
})
export class CrudProductoComponent {

  eventEdit:boolean;

  mostrarTabla:boolean=false;

  archivos:File[]=[];

  productModel:Producto;

  productos:Producto[]=[];

  categoria:Categoria=new Categoria();
  

  constructor(private httpClient : ProductoService,private route : Router) {

    this.productModel= new Producto();
    this.methodGet();
   }

   methodGet(){
    this.httpClient.getProducto().subscribe(data => {
      this.productos=data;
    })
   }
    
   acciones(accion:string,p:Producto,index?:number){
    if(accion == 'editar'){
      this.httpClient.saveProduct(p).subscribe();
    }
    if(accion == 'eliminar'){
      this.httpClient.deleteProduct(p.id_producto).subscribe(()=> this.productos.splice(index,1))
    }
  }

  verTabla(param:boolean){
    this.mostrarTabla = param;
  }


}
