import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {ProductoService} from '../../services/product.service'
import { Producto } from 'src/app/model/producto.model';

//Firebase
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface Item { nombre: string; url:string}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
    
  archivos:File[]=[]

  productModel:Producto=new Producto();

  mouseSobreElemento:boolean=false;

  sizefile:number =0;

  @Input() productos:Producto[]=[];
  
  @Input() eventEdit:boolean;
  
  @Output() productSeleccionado : EventEmitter<Producto>;
  @Output() evento : EventEmitter<number>;
  @Output() index : EventEmitter<number>;

  elementEdit:number=0;

  private itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;

  constructor(private productService:ProductoService,private afs: AngularFirestore) {
    this.productSeleccionado = new EventEmitter();
    this.evento = new EventEmitter();
    this.index = new EventEmitter();

    this.itemsCollection = afs.collection<Item>('img');
    this.items = this.itemsCollection.valueChanges();
    this.items.subscribe(data => console.log(data))
   }

   getProduct(p:Producto,btn?:number,index?:number){
     if(btn == 1){
        if(p.id_producto!=0){
          console.log(this.sizefile+"ESTO ES EL SIZE")
          if(this.sizefile>0){
            this.productService.cargarImagenes(this.archivos,p);
          }else{
            this.productService.saveProduct(p).subscribe();
          }
        }
        else{
          if(this.sizefile>0){

            this.productService.cargarImagenes(this.archivos,p);
            setTimeout(
              ()=>{
                this.productos.push(p);
                this.eventEdit=false;
              },2000
            );

          }else{
            alert("debe agregar una imagen");
          }
        }
     }
     else if(btn == 2){
        let id = p.id_producto;
        this.productService.deleteProduct(id).subscribe(data =>{
          this.productos.splice(index,1);
        });

     }else{
      this.productSeleccionado.emit(p);
     }
     this.limpiarModelo();
   }

   editar(id:number){
     if(this.elementEdit==id){
      return false;
     }
     else{
       return true;
     }
   }

  limpiarModelo(){
    this.productModel = new Producto();
  }

  
}
