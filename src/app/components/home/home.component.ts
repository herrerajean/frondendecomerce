import { Component, OnInit, Input } from '@angular/core';
import { ProductoService } from 'src/app/services/product.service';
import { Producto } from 'src/app/model/producto.model';
import {Router} from '@angular/router'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  productos:Producto[]=[]

  constructor(private httpClient : ProductoService, private router:Router) { 

    this.httpClient.getProducto().subscribe(data =>{
      this.productos=data;
    })
  }

  getProductoSeleccionado(p:Producto){
    this.router.navigate(['/producto',p.nombre_producto])
  }

}
