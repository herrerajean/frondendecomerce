import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/product.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent {

  products: any[] =[];

  constructor(private httpClient : ProductoService,private route: ActivatedRoute) { 
    this.route.params.subscribe( param =>{
      this.httpClient.getProductByCategory(param['id']).subscribe( data => {
        this.products = data;
      })
    })
  }
}
