import { Routes } from '@angular/router';

import { CategoriaComponent } from './components/categoria/categoria.component';

import { HomeComponent } from './components/home/home.component';
import { ProductSeleccionadoComponent } from './components/product-seleccionado/product-seleccionado.component';
import { CarritoComprasComponent } from './components/carrito-compras/carrito-compras.component';
import { CrudClientesComponent } from './components/cruds/crud-clientes/crud-clientes.component';
import { CrudProductoComponent } from './components/cruds/crud-producto/crud-producto.component';
import { AdminMenuComponent } from './components/admin/admin-menu/admin-menu.component';

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'producto/:name', component: ProductSeleccionadoComponent },
    { path: 'category/:id', component: CategoriaComponent },
    { path: 'admin', component: AdminMenuComponent },
    { path: 'admin/:manage', component: CrudProductoComponent },
    { path: 'admin/cliente', component: CrudClientesComponent },
    { path: 'cart', component: CarritoComprasComponent},
    { path: '**', pathMatch:'full',redirectTo:'home'}
    
];
