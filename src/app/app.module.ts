import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ProductsComponent } from './components/products/products.component';
import {HttpClientModule} from '@angular/common/http'

import {FormsModule} from '@angular/forms'

/* Servicios */
import {ProductoService} from './services/product.service';
import {PedidoService} from './services/order.service'


/* FIREBASE */
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { BuscarComponent } from './components/shared/buscar/buscar.component';
import { CategoriaComponent } from './components/categoria/categoria.component'
import { ROUTES } from './app.routes';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ProductSeleccionadoComponent } from './components/product-seleccionado/product-seleccionado.component';
import { CarritoComprasComponent } from './components/carrito-compras/carrito-compras.component';
import { environment } from 'src/environments/environment';
import { NgDropFilesDirective } from './directivas/ng-drop-files.directive';
import { CrudClientesComponent } from './components/cruds/crud-clientes/crud-clientes.component';
import { CrudProductoComponent } from './components/cruds/crud-producto/crud-producto.component';
import { ClienteService } from './services/client.service';
import { AdminMenuComponent } from './components/admin/admin-menu/admin-menu.component';
import { CrudPedidosComponent } from './components/cruds/crud-pedidos/crud-pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProductsComponent,
    BuscarComponent,
    CategoriaComponent,
    CrudProductoComponent,
    HomeComponent,
    ProductSeleccionadoComponent,
    CarritoComprasComponent,
    NgDropFilesDirective,
    CrudClientesComponent,
    AdminMenuComponent,
    CrudPedidosComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),
    AngularFireModule.initializeApp(environment.firebase), // imports firebase/app needed for everything
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireStorageModule,
    AngularFireAuthModule
  ],
  providers: [ProductoService,PedidoService,ClienteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
